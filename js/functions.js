/**
 * Created by l3yr0y on 5/12/15.
 */

function encodeURLCustom(params) {
    query_string = Object.keys(params.oauth).map(function (x) {
        return x + '%3D' + params.oauth[x];
    }).join('%26');
    query_string = 'GET&' + params.http_method + params.domain + params.request + params.filter.replace(/&/g, '%26') + '%26' + query_string;

    return query_string.replace(/\//g, '%2F').replace(/:/g, '%3A').replace('?', '&').replace(/\[/g, '%255B').replace(/]/g, '%255D').replace(/=/g, '%3D');
}

function getOauthSignature(params) {
    return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(
        encodeURLCustom(params),
        params.customer_secret));
}