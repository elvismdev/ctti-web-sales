 var today = new Date();
 var date = today.getFullYear() + '-' + (parseInt(today.getMonth()) + 1) + '-' + today.getDate();

 var testCodes = ['apptest', 'test10', 'test'];

// Shop.GrantCardoneTV.com
var witnation = {
    'http_method': 'https://',
    'domain': 'shop.grantcardonetv.com',
    'version': 'v3',
    'consumer_key': 'ck_d18375c60c47b06b3f57e6d36b23210a15668462', // From WP user profile configuration
    'customer_secret': 'cs_61b9c07878ab6ecb71c4208c797e64c1a8b8ba67', // From WP user profile configuration
    'request': '/wc-api/v3/reports/sales',
    'filter': '?filter[created_at_min]=' + date,
    'result': {}
};

// GrantCardone.com
var cardone = {
    'http_method': 'https://',
    'domain': 'www.grantcardone.com',
    'version': 'v2',
    'consumer_key': 'ck_ac46a54f69559bd03dc5c659f3753bb8', // From WP user profile configuration
    'customer_secret': 'cs_cefb91e1ea19a54a18c085702b0fe8b1', // From WP user profile configuration
    'request': '/wc-api/v2/reports/sales',
    'filter': '?filter[created_at_min]=' + date,
    'result': {}
};




// COUPONS

// Shop.GrantCardoneTV.com
var witnationCoupons = {
    'http_method': 'https://',
    'domain': 'shop.grantcardonetv.com',
    'version': 'v3',
    'consumer_key': 'ck_d18375c60c47b06b3f57e6d36b23210a15668462', // From WP user profile configuration
    'customer_secret': 'cs_61b9c07878ab6ecb71c4208c797e64c1a8b8ba67', // From WP user profile configuration
    'request': '/wc-api/v3/coupons',
    'filter': '?filter[meta_key]=usage_limit&filter[meta_value]=0',
    'result': {}
};

// GrantCardone.com
var cardoneCoupons = {
    'http_method': 'https://',
    'domain': 'www.grantcardone.com',
    'version': 'v2',
    'consumer_key': 'ck_ac46a54f69559bd03dc5c659f3753bb8', // From WP user profile configuration
    'customer_secret': 'cs_cefb91e1ea19a54a18c085702b0fe8b1', // From WP user profile configuration
    'request': '/wc-api/v2/coupons',
    'filter': '?filter[meta_key]=usage_limit&filter[meta_value]=0',
    'result': {}
};