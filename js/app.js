// Cross-Origin Request - This can be fixed by moving the resource to the same domain or enabling CORS.

(function () {
    var wooApp = angular.module('wooApp', []);

    angular.module('wooApp').factory('wooAppFactory', function($http) {
        function requests() {
            // HTTPS
            $http.get(witnation.http_method + witnation.domain + witnation.request + witnation.filter, {
                params: {
                    'consumer_key': witnation.consumer_key,
                    'consumer_secret': witnation.customer_secret
                }
            }).success(function (data, status) {
                witnation.result.response = data;
                witnation.result.status = status;
            }).error(function (data, status, headers, config) {
                witnation.result.response = data.errors[0];
                witnation.result.status = status;
            });

            // COUPONS
            // HTTPS
            $http.get(witnationCoupons.http_method + witnationCoupons.domain + witnationCoupons.request + witnationCoupons.filter, {
                params: {
                    'consumer_key': witnationCoupons.consumer_key,
                    'consumer_secret': witnationCoupons.customer_secret
                }
            }).success(function (data, status) {
                witnationCoupons.result.response = data;
                witnationCoupons.result.status = status;
            }).error(function (data, status, headers, config) {
                witnationCoupons.result.response = data.errors[0];
                witnationCoupons.result.status = status;
            });




            // HTTPS
            $http.get(cardone.http_method + cardone.domain + cardone.request + cardone.filter, {
                params: {
                    'consumer_key': cardone.consumer_key,
                    'consumer_secret': cardone.customer_secret
                }
            }).success(function (data, status) {
                cardone.result.response = data;
                cardone.result.status = status;
            }).error(function (data, status, headers, config) {
                cardone.result.response = data.errors[0];
                cardone.result.status = status;
            });

            // COUPONS
            // HTTPS
            $http.get(cardoneCoupons.http_method + cardoneCoupons.domain + cardoneCoupons.request + cardoneCoupons.filter, {
                params: {
                    'consumer_key': cardone.consumer_key,
                    'consumer_secret': cardone.customer_secret
                }
            }).success(function (data, status) {
                cardoneCoupons.result.response = data;
                cardoneCoupons.result.status = status;
            }).error(function (data, status, headers, config) {
                cardoneCoupons.result.response = data.errors[0];
                cardoneCoupons.result.status = status;
            });
        }

        return {
           requests: requests
       };
   });

wooApp.run(function (wooAppFactory) {
    wooAppFactory.requests();
});

wooApp.controller('wooAppCtrl', function ($scope, $interval, wooAppFactory) {
    var witnation_sales = 0;
    var cardone_sales = 0;

    var couponsWit = null;
    var couponsGc = null;

    $scope.now = today.toTimeString();
    $scope.witnation_error = '';
    $scope.witnationCoupons_error = '';
    $scope.cardone_error = '';
    $scope.cardoneCoupons_error = '';

    $interval(function () {
        $scope.now = new Date().toTimeString();
        wooAppFactory.requests();
        }, 60000); // 1 minute update time

    $scope.witnationResult = function () {
        if (witnation.result.status != 200) {
            if (witnation.result.response) {
                $scope.witnation_error = 'Error ocurred: ' + ' ' + witnation.result.status + ' ' + witnation.result.response.code + ' ' + witnation.result.response.message;
                witnation_sales = 0;
            }
        } else {
            if (witnation.result.response.sales) {
                $scope.witnation_error = '';
                witnation_sales = parseFloat(witnation.result.response.sales.total_sales);
            } else {
                $scope.witnation_error = 'Some error ocurred triying to get API info. Check for URL parameters. '  + witnation.result.response.errors[0].message;
                witnation_sales = 0;
            }
        }

        return witnation_sales;
    };

    $scope.witnationCouponsResult = function () {
        if (witnationCoupons.result.status != 200) {
            if (witnationCoupons.result.response) {
                $scope.witnationCoupons_error = 'Error ocurred: ' + ' ' + witnationCoupons.result.status + ' ' + witnationCoupons.result.response.code + ' ' + witnationCoupons.result.response.message;
                couponsWit = 'NO COUPONS';
            }
        } else {
            if (witnationCoupons.result.response.coupons) {
                $scope.witnationCoupons_error = '';
                witnation_coupons = witnationCoupons.result.response.coupons;
                var couponsWit = [];
                for (var key in witnation_coupons) {
                    if (witnation_coupons[key]['usage_limit'] != 1 && testCodes.indexOf(witnation_coupons[key]['code'])) {
                        couponsWit.push(witnation_coupons[key]);
                    }
                }
                witnationCoupons.result.response.coupons = null; // Avoid 2 times iterations
            } else {
                $scope.witnationCoupons_error = 'Some error ocurred triying to get API info. Check for URL parameters. '  + witnationCoupons.result.response.errors[0].message;
                couponsWit = 'NO COUPONS';
            }
        }

        return couponsWit;
    };

    $scope.cardoneResult = function () {
        if (cardone.result.status != 200) {
            if (cardone.result.response) {
                $scope.cardone_error = 'Error ocurred: ' + ' ' + cardone.result.status + ' ' + cardone.result.response.code + ' ' + cardone.result.response.message;
                cardone_sales = 0;
            }
        } else {
            if (cardone.result.response.sales) {
                $scope.cardone_error = '';
                cardone_sales = parseFloat(cardone.result.response.sales.total_sales);
            } else {
                $scope.cardone_error = 'Some error ocurred triying to get API info. Check for URL parameters. ' + cardone.result.response.errors[0].message;
                cardone_sales = 0;
            }
        }

        return cardone_sales;
    };

    $scope.cardoneCouponsResult = function () {
        var cardone_coupons = 'NO COUPONS';

        if (cardoneCoupons.result.status != 200) {
            if (cardoneCoupons.result.response) {
                $scope.cardoneCoupons_error = 'Error ocurred: ' + ' ' + cardoneCoupons.result.status + ' ' + cardoneCoupons.result.response.code + ' ' + cardoneCoupons.result.response.message;
            }
        } else {
            if (cardoneCoupons.result.response.coupons) {
                $scope.cardoneCoupons_error = '';
                cardone_coupons = cardoneCoupons.result.response.coupons;
                cardoneCoupons.result.response.coupons = null; // Avoid 2 times iterations
            } else {
                $scope.cardoneCoupons_error = 'Some error ocurred triying to get API info. Check for URL parameters. ' + cardoneCoupons.result.response.errors[0].message;
            }
        }

        return cardone_coupons;
    };

    $scope.total = function () {
        return witnation_sales + cardone_sales;
    };
});
})();