<?php
// Allow access to this application only from Grant Cardone's office based on IP address
// if (!(in_array(@$_SERVER['REMOTE_ADDR'], array('192.168.50.1', //Local Dev
//        '12.149.77.34', // CTTI Office IP
//        '12.149.77.10', // CTTI New Office Public IP
//        '216.189.180.93', // Elvis Home IP
//        '162.210.203.120', // Roberto
//        '107.72.164.83', // Roberto
//        '166.172.191.197', // Roberto
//        '162.210.203.13', // Roberto
//        '184.95.128.221', // Roberto
//        // '184.238.138.38' // Vaugh
//        )))
// ) {
//  header('HTTP/1.0 403 Forbidden');
//  exit('You are not allowed to access this application. Only from <a href="http://www.grantcardone.com">Grant Cardone</a> headquarters.');
// }
?>
<!DOCTYPE html>
<html lang="en" ng-app="wooApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Web Sales</title>
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <link rel="icon" sizes="192x192" href="imgs/android-icon.png">
  <link rel="apple-touch-icon" sizes="57x57" href="imgs/apple-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="imgs/apple-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="imgs/apple-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="imgs/apple-icon-144x144.png" />


  <!-- Bootstrap -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body ng-controller="wooAppCtrl">
    <div class="container">
      <h1 class="text-center">CTTI Day Web Sales</h1>
      <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
          <div class="row">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="witnation-logo"></span>

                <h3 class="panel-title">myGCTV.com</h3>
              </div>
              <div class="panel-body">
                <div class="alert alert-danger" role="alert" ng-show="witnation_error">{{witnation_error}}</div>
                <i class="fa fa-usd fa-2x"></i> <span class="total-money">{{witnationResult()|currency:''}}</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="gc-logo"></span>

                <h3 class="panel-title">GrantCardone.com</h3>
              </div>
              <div class="panel-body">
                <div class="alert alert-danger" role="alert" ng-show="cardone_error">{{cardone_error}}</div>
                <i class="fa fa-usd fa-2x"></i> <span class="total-money">{{cardoneResult()|currency:''}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div class="row">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title text-center">Stores Total</h3>
              </div>
              <div class="panel-body">
                <i class="fa fa-usd fa-2x"></i> <span class="total-money">{{total()|currency:''}}</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="panel panel-default">
              <div class="panel-body bg-primary">
                <input type="text" ng-model="yourGoal" placeholder="Set a goal" class="form-control">
                <div class="clearfix visible-xs-block"></div>
                <h3 class="goal"><i class="fa fa-usd fa-2x"></i> {{yourGoal}}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title text-center">Coupons myGCTV.com</h3>
            </div>
            <div class="panel-body">
              <div class="alert alert-danger" role="alert" ng-show="witnationCoupons_error">{{witnationCoupons_error}}</div>
              <span class="total-coupons">
                <ul>
                  <li data-ng-repeat="coupon in witnationCouponsResult()">
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5"><i class="fa fa-ticket"></i> {{coupon.code | uppercase}}</div>
                      <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">{{coupon.description}}</div>
                    </div>
                  </li>
                </ul>
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title text-center">Coupons GrantCardone.com</h3>
            </div>
            <div class="panel-body">
              <div class="alert alert-danger" role="alert" ng-show="cardoneCoupons_error">{{cardoneCoupons_error}}</div>
              <span class="total-coupons">
                <ul>
                  <li data-ng-repeat="coupon in cardoneCouponsResult()">
                    <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-5 col-xs-5"><i class="fa fa-ticket"></i> {{coupon.code | uppercase}}</div>
                      <div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">{{coupon.description}}</div>
                    </div>
                  </li>
                </ul>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="text-center">
      <h4>Last update: {{now}}</h4>
    </div>
  </div>

  <!-- Include Local JS -->
  <script src="bower_components/angular/angular.min.js"></script>
  <script src="js/hmac-sha256.js"></script>
  <script src="js/enc-base64-min.js"></script>
  <script src="js/configuration.js"></script>
  <script src="js/functions.js"></script>
  <script src="js/app.js"></script>

</body>
</html>